<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="../media/css/style.css">
	<link rel="stylesheet" href="../media/css/footer-dark.css">
    <title>Tienda Virtual</title>
  </head>
  <body>
    <div class="container contener-principal">
       	<!--header-->
		<?php
			require('../components/header.php');
		?>
		
		<div class="row">
			<div class="col-md-9">
                <div class="jumbotron detalle-producto">
                    <img src=".." alt="" srcset="">
                    <h1 class="display-4">Title</h1>
                    <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                </div>
			</div>
			<div class="col-md-3">
                <div class="precio">
                    <h4>Comprar</h4>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Precio:</span>
                            <span class="input-group-text">$ 0.00</span>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">U/D:</span>
                            <span class="input-group-text">5</span>
                        </div>
</div>

                </div>

            </div>  
	    </div>

		<!--Footer-->
		<?php
			require('../components/footer.php');
		?>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $('.carousel').carousel();
    </script>
  </body>
</html>