<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner carrusel-cabezera">
        <div class="carousel-item active">
            <img src="/media/img/ropa.webp" class="d-block w-100" alt="banner-ropa">
        </div>
        <div class="carousel-item">
            <img src="/media/img/novedades.jpg" class="d-block w-100" alt="banner-novedades">
        </div>
        <div class="carousel-item">
            <img src="/media/img/chaquetas.jpg" class="d-block w-100" alt="banner-chaquetas">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    	<span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div class="barrar-navegacion-principal">
    <!-- Image and text -->
    <nav class="navbar navbar-light navbar-expand-md bg-light">
        <a class="navbar-brand" href="#">
            <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
            Tienda Virtual
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
                    
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/proyecto-db/index.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Cate 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Cate 2</a>
				</li>
				<li class="nav-item">
                    <a class="nav-link" href="#">Cate 3</a>
                </li>  
                        <!-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Menu Usuario
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
						</li>
						<li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Menu Empleado
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
						</li>
						<li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Menu Administrador
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
						</li> -->
						
			</ul>
			<div class="btn-group btn-group-toggle" data-toggle="buttons">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalInicioSesion">Ingresar</button>
				<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalRegistro">Registrate!</button>
			</div>
        </div>
    </nav>
</div>
<!-- Modal Iniciar Sesion-->
<div class="modal fade" id="modalInicioSesion" tabindex="-1" role="dialog" aria-labelledby="modalInicioSesionTitle" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-centered" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="modalInicioSesionTitle">Iniciar Sesion</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" name="email" placeholder="Email" require>
					</div>
					<div class="form-group">
						<label for="password">Contraseña</label>
						<input type="password" class="form-control" name="password" placeholder="Contraseña" require>
					</div>
					<button type="submit" class="btn btn-primary">Ingresar</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
				</form>
      		</div>
    	</div>
  	</div>
</div>
<!-- Modal Registro-->
<div class="modal fade" id="modalRegistro" tabindex="-1" role="dialog" aria-labelledby="modalRegistroTitle" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-centered" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="modalRegistroTitle">Registro de Usuario</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
			<form>
      			<div class="modal-body">
					<div class="form-group">
						<label for="nombres">Nombres</label>
						<input type="text" class="form-control" name="nombres" placeholder="Nombres" require>
					</div>
					<div class="form-group">
						<label for="apellidos">Apellidos</label>
						<input type="text" class="form-control" name="apellidos" placeholder="Apellidos" require>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" name="email" placeholder="Email" require>
					</div>
					<div class="form-group">
						<label for="password">Contraseña</label>
						<input type="password" class="form-control" name="password" placeholder="Contraseña" require>
					</div>
					<div class="form-group">
						<label for="password2">Repetir Contraseña</label>
						<input type="password" class="form-control" name="password2" placeholder="Repetir Contraseña" require>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Registrarse</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
				</div>
			</form>
    	</div>
  	</div>
</div>