<div class="footer-dark">
    <footer>
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-6 col-md-3 item">
                    <h3>Servicios</h3>
                    <ul>
    	                <li>
    	                	<a href="http://">Diseño Web</a>
    	                </li>
    	                <li>
                            <a href="http://">Desarrollo</a>
                        </li>
                        <li>
                            <a href="http://">Alojamiento</a>
                        </li>
                    </ul>
    			</div>
    			<div class="col-sm-6 col-md-3 item">
                    <h3>Aserca</h3>
                    <ul>
                        <li>
                            <a href="http://">Compañia</a>
                        </li>
                        <li>
                            <a href="http://">Equipo</a>
                        </li>
                        <li>
                            <a href="http://">Mas+</a>
                        </li>
                    </ul>
    			</div>
    			<div class="col-md-6 item text">
                    <h3>Tienda Virtual</h3>
                    <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac sem lacus. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo.</p>
    			</div>
    			<div class="col item social">
                    <a href="http://">
                        <i class="icon ion-social-facebook"></i>
                    </a>
                    <a href="http://">
                        <i class="icon ion-social-twitter"></i>
                    </a>
                    <a href="http://">
                        <i class="icon ion-social-snapchat"></i>
                    </a>
                    <a href="http://">
                        <i class="icon ion-social-instagram"></i>
                    </a>
                </div>	
            </div>
       		<p class="copyright">Tienda Virtual 2017</p>
     	</div>
    </footer>
</div>