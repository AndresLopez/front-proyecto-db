<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="media/css/style.css">
	<link rel="stylesheet" href="media/css/footer-dark.css">
    <title>Tienda Virtual</title>
  </head>
  <body>
    <div class="container contener-principal">
       	<!--header-->
		<?php
			require('components/header.php');
		?>
		<div class="container">
			<div class="row">
				<div class="col-md-3 producto-card">
					<div class="card">
						<img class="card-img-top w-100 d-block" src="..." alt="Imagen">
						<div class="card-body">
							<h4 class="text-center card-title">Titulo</h4>
							<p class="text-justify card-text">Test</p>
						</div>
						<div class="botones-compras">
						<a href="public/detalle-producto.php"><button class="btn btn-info btn-sm boton-ver" type="button">Ver</button></a>
							<button class="btn btn-warning btn-sm boton-agregar" type="button" disabled>Agregar</button>
						</div>
					</div>
				</div>
				<div class="col-md-3 producto-card">
					<div class="card">
						<img class="card-img-top w-100 d-block" src="..." alt="Imagen">
						<div class="card-body">
							<h4 class="text-center card-title">Titulo</h4>
							<p class="text-justify card-text">Test</p>
						</div>
						<div class="botones-compras">
							<a href="public/detalle-producto.php"><button class="btn btn-info btn-sm boton-ver" type="button">Ver</button></a>
							<button class="btn btn-warning btn-sm boton-agregar" type="button" disabled>Agregar</button>
						</div>
					</div>
				</div>
				<div class="col-md-3 producto-card">
					<div class="card">
						<img class="card-img-top w-100 d-block" src="..." alt="Imagen">
						<div class="card-body">
							<h4 class="text-center card-title">Titulo</h4>
							<p class="text-justify card-text">Test</p>
						</div>
						<div class="botones-compras">
							<a href="public/detalle-producto.php"><button class="btn btn-info btn-sm boton-ver" type="button">Ver</button></a>
							<button class="btn btn-warning btn-sm boton-agregar" type="button" disabled>Agregar</button>
						</div>
					</div>
				</div>
				<div class="col-md-3 producto-card">
					<div class="card">
						<img class="card-img-top w-100 d-block" src="..." alt="Imagen">
						<div class="card-body">
							<h4 class="text-center card-title">Titulo</h4>
							<p class="text-justify card-text">Test</p>
						</div>
						<div class="botones-compras">
							<a href="public/detalle-producto.php"><button class="btn btn-info btn-sm boton-ver" type="button">Ver</button></a>
							<button class="btn btn-warning btn-sm boton-agregar" type="button" disabled>Agregar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Footer-->
		<?php
			require('components/footer.php');
		?>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $('.carousel').carousel();
    </script>
  </body>
</html>